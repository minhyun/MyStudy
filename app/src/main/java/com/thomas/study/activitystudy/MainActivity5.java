package com.thomas.study.activitystudy;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity5 extends AppCompatActivity {

    //항상 메소드를 호출후 그상태로 변함

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        if(savedInstanceState !=null){
            //비정상 종료 후 재구동
            //값 복원
        }else{
            //정상구동
        }
        //액티비티가 처음 만들어졌을때 호출됨
        //이전 상태가 저장되어 있는경우 번들객체를 참조하여 이전상태 복원 가능
        //데이터를 한번 가져와서 쓸대는 여기서
    }

    @Override
    protected void onStart() {
        super.onStart();
        //액티비티가 화면에 보이기 바로 전에 호출됨
        //화면에 필요한 처리를할떄
    }

    @Override
    protected void onResume() {
        super.onResume();
        //화면 상에 액티비티가 보이면서 포커스를 가지고 있는 상태
        //데이터를 화면에 보일때마다 새롭게 보일때는 여기서
    }

    @Override
    protected void onPause() {
        super.onPause();
        //사용자에게 보이긴하지만 다른 액티비티가 위에있어서 포커스를 받지 못하는 상태
        //화면전체가 가려져있는지 확인 후 onStop실행
        //다시 구동상태로 돌아가려면 onResume()을 호출
    }

    @Override
    protected void onStop() {
        super.onStop();
        //화면전체가 가려져 보이지 않는 상태
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //액티비티가 소멸되어 없어지기전에 호출
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //onStop에서 다시 구동될때 onRestart()실행되고 onStart()를 실행
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------------

    //activitiy가 갑자기 죽을 것을 대비해서 activitiy의 상태를 저장하기 위해 호출되는 함수
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
    //activitiy가 갑자기 죽었을때 저장한 정보를 복원하기 위한 함수
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    //activitiy가 최초 생성되어 필요한 작업이 완료 되었을때 호출
    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
    }
    //activitiyrk Resume이 완료되었을때 호출되는 함수
    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
    //액티비티가 백그라운드로갔을때 (포커스를잃었을때)
    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
    }
    //back key가 눌렸을때 호출되는 함수
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    //activitiy가 생성되어 있는 상태에서 다른 곳에서 activitiy를 다시 띄울때 intent를 받기위한 함수
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
    //호출한 acitivitiy에서 넘겨준 값을 받기위한 함수
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    //setContentView나 addContentView로 activitiy의 ContentView가 변경되었을 때 호출되는 함수
    @Override
    public void onContentChanged() {
        super.onContentChanged();
    }
    //Activitiy의 LayoutInflater에 의해 View가 생성되면 호출되는 함수
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }
}
