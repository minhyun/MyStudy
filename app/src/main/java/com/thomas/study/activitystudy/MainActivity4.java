package com.thomas.study.activitystudy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity4 extends AppCompatActivity {
    @BindView(R.id.act4_tv1) TextView act4_tv1;
    @BindView(R.id.act4_tv2) TextView act4_tv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        ButterKnife.bind(this);

        Intent intent =getIntent();
        MyData data =(MyData)intent.getSerializableExtra("OBJECT");
        data.getName();
        data.getAge();
        act4_tv1.setText(data.getName());
        act4_tv2.setText(String.valueOf(data.getAge()));
    }
}
