package com.thomas.study.activitystudy;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity2 extends AppCompatActivity {
    @BindView(R.id.act2_tv1) TextView act2_tv1;
    @BindView(R.id.act2_tv2) TextView act2_tv2;

    public final static String INPUT_MESSAGE="inputmessage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        Intent intent=getIntent();
        String name = intent.getStringExtra("name");//, 뒤에 값은 기본값 (이게없으면 데이터 없을시 오류)
        int age = intent.getIntExtra("age",-1);// , 뒤에 값은 기본값 (이게없으면 데이터 없을시 오류)
        boolean isOk = intent.getBooleanExtra("ok",true);//, 뒤에 값은 기본값 (이게없으면 데이터 없을시 오류)
        String text = intent.getStringExtra(INPUT_MESSAGE);
        act2_tv1.setText("이름:"+name+"나이:"+age);
        act2_tv2.setText(text);

    }

    @OnClick(R.id.act2_btn2) void click(){
        finish();
    }
}
