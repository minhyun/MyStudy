package com.thomas.study.activitystudy;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class MainActivity extends AppCompatActivity{
    @BindView(R.id.main_et1) EditText main_et1;
    @BindView(R.id.main_tv1) TextView main_tv1;
    public static final int REQUEST_ACTIVITY=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.main_btn1,R.id.main_btn2,R.id.main_btn3,R.id.main_btn4,R.id.main_btn5}) void click(View view){
        switch (view.getId()){
            case R.id.main_btn1 :
                //데이터 보내기
                Intent i = new Intent(MainActivity.this,MainActivity2.class);
                i.putExtra("name","min");
                i.putExtra("age",30);
                i.putExtra("ok",true);
                i.putExtra(MainActivity2.INPUT_MESSAGE,main_et1.getText().toString());
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //액티비티 마다 자신만의 태스크를 가짐 a에서 b띄우면 a에 종속됨 //자신고유의 태스크로 떠라 할때 씀(있으면 재활용)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//a-b-c 순서대로 열때 c-a로 갈때 b와c를 지움 //a는 다시 띄워지니 방지하기위해 SING_TOP과 같이써라
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);//회원가입 후 로그인해서 메인으로 갔을시 자주 씀  //뜨기 이전의 모든 태스크 지움
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);//activitiy가 pause되면 acivitiy를 종료함
                i.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);//설정된 Flag로 activitiyStack을 정리
                i.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);//(a)에서 startAcitivityForResult로 (b)에게 보내고 (b)는FORWARD_RESULT써서 (c)에게 보낸 후(c)는 setResult로 (a)에게 전달
                startActivity(i);
                break;

            case R.id.main_btn2:
                //데이터 주고받기
                Intent i2 = new Intent(MainActivity.this,MainActivity3.class);
                String input = main_et1.getText().toString();
                i2.putExtra(MainActivity3.MA3,input);
                startActivityForResult(i2,REQUEST_ACTIVITY);
                break;

            case R.id.main_btn3:
                //객체 직렬화하여 전송
                MyData mydata = new MyData();
                mydata.setName("오잉");
                mydata.setAge(1);
                Intent i3 = new Intent(MainActivity.this,MainActivity4.class);
                i3.putExtra("OBJECT",mydata);
                startActivity(i3);
                break;

            case R.id.main_btn4:
                startActivity(new Intent(MainActivity.this,MainActivity5.class));
                break;
            

            case R.id.main_btn5 :
                break;
        }

    }

    //acitivity3 setResult 로부터 받은 값
    @Override                               //startActivityForResult에서 사용한 requestCode값, Result_ok or Result_fail, 데이터
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ACTIVITY && resultCode == Activity.RESULT_OK){
            String result = data.getStringExtra(MainActivity3.RESULT_MESSAGE);
            main_tv1.setText(result);
        }
    }
}
