package com.thomas.study.activitystudy;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity3 extends AppCompatActivity {
    public static final String MA3="MainActivity3";
    public static final String RESULT_MESSAGE="resultmessage";
    @BindView(R.id.act3_et2) EditText act3_et2;
    @BindView(R.id.act3_tv3) TextView act3_tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        ButterKnife.bind(this);
        Intent e = getIntent();
        String re=e.getStringExtra(MA3);
        act3_tv3.setText(re);
    }
    @OnClick(R.id.act3_btn3) void click(){
        Intent data = new Intent();
        data.putExtra(RESULT_MESSAGE,act3_et2.getText().toString());
        setResult(Activity.RESULT_OK,data);
        finish();
    }
}
